import "javascript/PersistentData.js" as PersistentData

import com.nokia.meego 1.0
import QtQuick 1.1
import QtMultimediaKit 1.1

Page {
    id: aboutpage
    orientationLock: PageOrientation.LockPortrait
    tools:aboutpagelayout1
    property string abouttext: "Version 4.0.4 Meego\nRelease date: 09/12/2012\nMade by Léo\nskiwebcams@ovi.com\n------------\nWebcams images aren't my\nproperty\nContact me for any\nsuggestion/complain\n------------\nOnline audio streaming does not work on every connection, please check your operator contract and/or try on wifi. Streaming Radio: Hot Radio Grenoble\n------------\n"

    Background{
        id:background
        Component.onCompleted: background.changex(0);
    }

    Rectangle {
        id:helpScreen
        visible: true
        width:parent.width


        anchors{
            top:parent.top
            topMargin: parent.height/16
            bottom:parent.bottom
        }

        color: "transparent"

        Flickable {
            id: flickAreamoretext
            anchors.fill: parent
            contentWidth: helpText.width; contentHeight: skiwebcamslogo.height+helpText.height+buttoninfosplash.height+10
            flickableDirection: Flickable.VerticalFlick
            clip: true

            Image{
                id:skiwebcamslogo
                scale: 0.9
                opacity: 1
                smooth:true
                source: "qrc:///skiwebcams.png"
            }

            Text{
                id: helpText
                anchors.top: skiwebcamslogo.bottom
                wrapMode: TextEdit.Wrap
                width:helpScreen.width;
                // readOnly:true

                color: "#ffffff"
                text: abouttext
                style: Text.Sunken
                font.bold: true
                styleColor: "#000000"
                horizontalAlignment: Text.AlignHCenter
                font.family: skiWebWindow.police
                font.pixelSize: 30


            }

            Text {
                id: releasedate
                anchors.top: helpText.bottom
                anchors.topMargin: 5
                width: parent.width
                text: PersistentData.releasedatestring
                color: "#ffffff"
                styleColor: "#000000"
                horizontalAlignment: Text.AlignHCenter
                font.bold: true
                style: "Sunken"
                font.pixelSize: 30
                font.family: skiWebWindow.police
            }




        }
    }

    ToolBarLayout {
        id:aboutpagelayout1
        visible: true
        ToolIcon {
            platformIconId: "toolbar-back"
            onClicked: {
                pageStack.pop();
            }
        }
        Button {
            id: buttoninfosplash
            anchors{
                right:parent.right
                rightMargin: aboutpage.width/12
            }
            text: "More"
            onClicked: {pageStack.push(Qt.resolvedUrl("InfoSplashPage.qml"),{idtools:0})
            }
        }

    }

}
