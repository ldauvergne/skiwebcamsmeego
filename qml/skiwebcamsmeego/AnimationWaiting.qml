// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {
 id:wp7waitanim

    property int size: 15
    property bool working: true
    property color color: "white"

    height: size
    visible: true
    Repeater {
        model: 5

        Rectangle {
            id: rect
            color: work_.color
            width: work_.size
            height: work_.size
            radius: 2.0

            property int factor: work_.size * 1.75
            property int me: (index - 2.5) * factor
            property int x0: - factor * 3 - me -300
            property int x1: work_.width + factor * 3 - me +300

            SequentialAnimation {
                running: work_.working
                loops: Animation.Infinite
                PropertyAction { target: rect; property: "x"; value: - work_.size }  // invisible
                PauseAnimation { duration: 200 + 100 * index }
                NumberAnimation {
                    target: rect; property: "x"; from: x0; to: x1
                    duration: 3000
                    easing.type: Easing.OutInExpo
                }
                PauseAnimation { duration: 2000 - 100 * index }
            }
        }
    }
}
