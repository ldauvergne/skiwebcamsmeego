import "javascript/IntelInterface.js" as IntelInterface
import "javascript/Storage.js" as Storage
import "javascript/SaveandLoad.js" as SaveandLoad
import "javascript/PersistentData.js" as PersistentData
import "javascript/IntelParser.js" as IntelParser
import "javascript/IntelFavourites.js" as IntelFavourites

import com.nokia.meego 1.0
import QtQuick 1.1
import QtMultimediaKit 1.1
import com.nokia.extras 1.0

Page {
    id: favbrowsingpage
    orientationLock: PageOrientation.LockPortrait
    tools:favbrowsingpagelayout1
    property int movex:0
    property string textcolor:"white"

    Component.onCompleted: {

        IntelFavourites.loadAllFavs();
    }

    Rectangle{
        id: fabrowser
        opacity:1
        anchors.fill: parent


        Background{
            id:background
            Component.onCompleted: background.changex(0);
        }


        ////////////////// MISE EN PLACE DES VUES
        VisualItemModel {
            id: itemModel




            //Second hub tile - Stations menu
            Rectangle {
                id: favrect
                width: favbrowsingpage.width*2; height:view.height
                color: "#00000000"
                opacity: 1

                Text {
                    id: headingText
                    anchors.leftMargin: 10
                    clip: false

                    text: PersistentData.apptitle

                    anchors.fill: parent
                    anchors.topMargin: 15
                    wrapMode: Text.NoWrap
                    font { family:skiWebWindow.police; pointSize: 60}
                    smooth: true
                    style: Text.Raised
                    opacity:1
                    color: textcolor
                    z:2
                }

                Text {
                    color: textcolor;
                    text: "Favourites"
                    font { family:skiWebWindow.police; bold: false; pointSize: 40}
                    smooth: true
                    style: Text.Raised
                    anchors.leftMargin: 10
                    anchors {top: parent.top; left:parent.left; topMargin:110}
                    opacity:1
                    z:2}

                Rectangle {
                    id: rectangle2
                    height: 180
                    width: favbrowsingpage.width*2;
                    color: "#000"
                    opacity: 1
                    z:0
                    Image{
                        id: backgroundtop2
                        opacity: 0.6
                        height:180
                        source: "backgroundtop.jpg"
                        //fillMode: Image.PreserveAspectCrop
                        anchors.fill: parent
                    }
                }
                Rectangle {
                    z:-1
                    color: "#00000000"
                    anchors{ left:parent.left; right:parent.right; bottom:parent.bottom; top:parent.top
                        leftMargin: 10; rightMargin:10; bottomMargin:10; topMargin:200 }

                    VisualDataModel {
                        id: galleryItemModel

                        model :ListModel {
                            id: albumFavModel
                        }

                        delegate:
                            Rectangle{
                            width: itemText.width+10; height: itemText.height+5
                            color: "#00000000"
                            id:menuItem
                            Text{id:itemText
                                x:10
                                y:7
                                smooth: true
                                style: Text.Raised
                                font{family:skiWebWindow.police; bold: true ;pointSize: 28} color: "white"
                                text: album
                            }
                            MouseArea{
                                anchors.fill: parent
                                onClicked: {albumFavList.currentIndex = albumFavList.indexAt(menuItem.x, menuItem.y)
                                }
                                onDoubleClicked: {view.currentIndex=1}
                            }
                        }
                    }
                    ListView {
                        id: albumFavList
                        boundsBehavior: Flickable.DragAndOvershootBounds
                        anchors.fill: parent;
                        model: galleryItemModel
                        orientation: ListView.Vertical
                        flickDeceleration: 2000
                        cacheBuffer: 1000
                        highlightFollowsCurrentItem: true
                        highlight: Rectangle { color: "#00000000"; border {color:"#ffffff"; width:3} radius: 5}
                        currentIndex: -1

                        onCurrentIndexChanged: {
                            if(currentIndex!==-1)
                            {
                                highlightItem.width=currentItem.width+7;

                                IntelFavourites.createFavImages(currentIndex);
                                //SaveandLoad.favcountry();

                            }
                        }
                    }
                }
            }




            //Third hub page - the pictures
            Rectangle {
                id: galleryRect
                width: favbrowsingpage.width; height: view.height
                color: "#00000000"
                opacity: 1


                Text {
                    id: galleryText;

                    color:textcolor;

                    font {family:skiWebWindow.police; bold: false; pointSize: 40}
                    smooth: true

                    style: Text.Raised
                    anchors.leftMargin: 10
                    anchors {top: parent.top; left:parent.left; topMargin:110}
                    opacity:1
                }

                VisualDataModel {
                    id: dataModel
                    model: ListModel{
                        id:innerModel
                    }

                    delegate:

                        Image {
                        id: imagebro
                        width: photoGridView.cellWidth-10
                        height: photoGridView.cellHeight-10
                        sourceSize.width: photoGridView.cellWidth-10
                        sourceSize.height:photoGridView.cellHeight-10
                        clip: false
                        fillMode: Image.Stretch
                        source: url
                        BusyIndicator {
                            id: waiting_imagebro
                            x: (parent.width/2)-20
                            y: (parent.height/2)-20
                            width: 40
                            height: 40
                            running: true
                            visible: true
                        }
                        onStatusChanged: if (imagebro.status == Image.Ready){ waiting_imagebro.visible=false}


                        MouseArea{
                            anchors.fill: parent
                            onClicked: {
                                //console.log(PersistentData.albumsbig[countryList.currentIndex][albumList.currentIndex])
                                pageStack.push(Qt.resolvedUrl("ImageBig.qml"), { imagesource: imagebro.source, countryListcurrentIndex: IntelFavourites.getfav(albumFavList.currentIndex)[0], albumListcurrentIndex:IntelFavourites.getfav(albumFavList.currentIndex)[1]});


                            }

                        }
                    }
                }

                GridView {
                    id: photoGridView
                    model: dataModel
                    cacheBuffer: 5000

                    anchors{
                        left: parent.left
                        leftMargin:10

                        right: parent.right
                        rightMargin: 10

                        bottom: parent.bottom
                        bottomMargin: 10

                        top: parent.top
                        topMargin: 200

                    }


                    cellWidth: (parent.width/2)-10
                    cellHeight: cellWidth
                    snapMode: GridView.NoSnap
                    boundsBehavior: Flickable.StopAtBounds
                    clip: true
                }

            }

        }

        ////////////////// GESTION DES VUES
        ListView {
            id: view
            visible:true

            model: itemModel
            orientation: ListView.Horizontal
            snapMode: ListView.SnapOneItem;
            flickDeceleration: 500
            cacheBuffer: 2000;

            boundsBehavior: Flickable.DragOverBounds

            property int previousX;

            x: 0

            width: favbrowsingpage.width
            height: favbrowsingpage.height
            contentWidth: 0



            onMovementStarted: previousX=contentX
            onContentXChanged:{ if ((view.contentX >= galleryRect.x) && (view.contentX <=(galleryRect.x+galleryRect.width-30)))
                {
                    if (previousX>contentX && contentX<(galleryRect.x+50)) {
                        setSnapMode(true);
                    }else {
                        setSnapMode(false);
                    }
                } else {
                    setSnapMode(true);
                }

            }


            function setSnapMode(on){
                if (on===true){
                    view.snapMode=ListView.SnapOneItem;
                    view.highlightRangeMode= ListView.StrictlyEnforceRange;
                } else if (on===false) {
                    view.snapMode=ListView.NoSnap;
                    view.highlightRangeMode= ListView.NoHighlightRange;
                }
            }
        }






    }


    ToolBarLayout {
        id:favbrowsingpagelayout1
        visible: true
        ToolIcon {
            platformIconId: "toolbar-back"
            onClicked: {

                    pageStack.pop();

                }
            }


        ToolIcon {
            platformIconId: "toolbar-view-menu"
            anchors.right: (parent === undefined) ? undefined : parent.right
            onClicked: (favbrowsingpagemenu.status === DialogStatus.Closed) ? favbrowsingpagemenu.open() : mainbrowsingpagemenu.close()
        }
    }

    InfoBanner {
        id:bannerFav
    }

    Menu {
        id: favbrowsingpagemenu
        visualParent: pageStack
        MenuLayout {
            MenuItem { text: qsTr("About")
                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml")); }

            MenuItem { text: qsTr("Clear all favourites")
                onClicked: {
                    if(albumFavList.count!==0){
                        IntelFavourites.removeAllFavs()
                        bannerFav.text="All Favourites removed."
                        bannerFav.iconSource="Refresh.png"

                        bannerFav.show();
                    }
                    else{
                        bannerFav.text="No favourites to remove."
                        bannerFav.iconSource="Warning.png"

                        bannerFav.show();
                    }
                }
            }

            MenuItem { text: qsTr("Erase from favourites")
                onClicked: {
                    if(albumFavList.currentIndex!==-1){
                        IntelFavourites.removeFavByIndex(albumFavList.currentIndex)
                        bannerFav.text="Favourite removed."
                        bannerFav.iconSource="Refresh.png"

                        bannerFav.show();
                    }
                    else{
                        bannerFav.text="No favourite to remove."
                        bannerFav.iconSource="Warning.png"

                        bannerFav.show();
                    }
                }
            }
        }
    }


}



