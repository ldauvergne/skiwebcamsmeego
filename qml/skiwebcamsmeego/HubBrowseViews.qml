import "javascript/IntelInterface.js" as IntelInterface
import "javascript/Storage.js" as Storage
import "javascript/SaveandLoad.js" as SaveandLoad
import "javascript/PersistentData.js" as PersistentData
import "javascript/IntelParser.js" as IntelParser


import com.nokia.meego 1.0
import QtQuick 1.1

Rectangle{
    id: mainbrowser
    opacity:1
    anchors.fill: parent

    Background{
        id:background
    }

    function getselectedstationindex(){
        return albumList.currentIndex
    }
    function getselectedcountryindex(){
        return countryList.currentIndex
    }

    ////////////////// MISE EN PLACE DES VUES
    VisualItemModel {
        id: itemModel

        //First hub tile - Countries list
        Rectangle {
            id: countryrect
            width: mainpagewindow.width*2; height: view.height
            color: "#00000000"
            visible:true




            Text {
                id: headingText
                anchors.leftMargin: 10
                clip: false

                text: PersistentData.apptitle

                anchors.fill: parent
                anchors.topMargin: 15
                wrapMode: Text.NoWrap
                font { family:skiWebWindow.police; pointSize: 60}
                smooth: true
                style: Text.Raised
                opacity:1
                color: "white"
                z:2
            }

            Text {id: countriesText; color: textColor; text: "Countries"
                font {family:skiWebWindow.police; bold: false; pointSize: 40}
                smooth: true
                style: Text.Raised
                anchors.leftMargin: 10
                anchors {top: parent.top; left:parent.left; topMargin:110}
                opacity: 1
                z:2}


            Rectangle {
                id: rectangle1
                height: 180
                width: mainpagewindow.width*2;
                color: "#000"
                opacity: 1
                z:0
                Image{
                    id: backgroundtop
                    opacity: 0.6
                    height:180
                    source: "backgroundtop.jpg"
                    anchors.fill: parent
                    //fillMode: Image.PreserveAspectFit
                }
            }

            Rectangle {
                z:-2
                color: "#00000000"

                anchors{ left:parent.left; right:parent.right; bottom:parent.bottom; top:parent.top
                    leftMargin: 10; rightMargin:10; bottomMargin:10; topMargin:200 }

                VisualDataModel {
                    id: countryItemModel
                    model :ListModel {
                        id: countryModel
                    }

                    delegate:
                        Rectangle{
                        width: countryText.width+10; height: countryText.height+10
                        color: "#00000000"
                        id:countryItem
                        Text{
                            id:countryText
                            smooth: true
                            style: Text.Raised
                            x:10
                            y:8
                            font{family:skiWebWindow.police; bold: true ;pointSize: 28} color: textColor
                            text: pays
                        }
                        MouseArea{
                            anchors.fill: parent
                            onClicked: {countryList.currentIndex = countryList.indexAt(countryItem.x, countryItem.y)}
                            onDoubleClicked: {view.currentIndex=1}
                        }
                    }
                }

                ListView {
                    id: countryList
                    boundsBehavior: Flickable.DragAndOvershootBounds
                    anchors.fill:parent
                    model: countryItemModel
                    orientation: ListView.Vertical
                    flickDeceleration: 2000
                    cacheBuffer: 1000
                    highlightFollowsCurrentItem: true
                    highlight: Rectangle { color: "#00000000"; border {color:"#ffffff"; width:3} radius: 5}
                    currentIndex: -1


                    Component.onCompleted: IntelInterface.createPays();

                    onCurrentIndexChanged: {
                        if(currentIndex!==-1)
                        {
                            highlightItem.width=currentItem.width+7;

                            albumList.currentIndex=-1
                            IntelInterface.createAlbums(currentIndex);

                            /*
                            SaveandLoad.favcountry();
                            */
                        }
                    }
                }
            }
        }


        //Second hub tile - Stations menu
        Rectangle {
            id: stationrect
            width: mainpagewindow.width*2; height:view.height
            color: "#00000000"
            opacity: 1


            Text {id: locationText; color: textColor;
                font { family:skiWebWindow.police; bold: false; pointSize: 40}
                smooth: true
                style: Text.Raised
                anchors.leftMargin: 10
                anchors {top: parent.top; left:parent.left; topMargin:110}
                opacity:1
                z:2}

            Rectangle {
                id: rectangle2
                height: 180
                width: mainpagewindow.width*2;
                color: "#000"
                opacity: 1
                z:0
                Image{
                    id: backgroundtop2
                    opacity: 0.6
                    height:180
                    source: "backgroundtop2.jpg"
                    //fillMode: Image.PreserveAspectCrop
                    anchors.fill: parent
                }
            }
            Rectangle {
                z:-1
                color: "#00000000"
                anchors{ left:parent.left; right:parent.right; bottom:parent.bottom; top:parent.top
                    leftMargin: 10; rightMargin:10; bottomMargin:10; topMargin:200 }

                VisualDataModel {
                    id: galleryItemModel

                    model :ListModel {
                        id: albumModel
                    }

                    delegate:
                        Rectangle{
                        width: itemText.width+10; height: itemText.height+5
                        color: "#00000000"
                        id:menuItem
                        Text{id:itemText
                            x:10
                            y:7
                            smooth: true
                            style: Text.Raised
                            font{family:skiWebWindow.police; bold: true ;pointSize: 28} color: textColor
                            text: album
                        }
                        MouseArea{
                            anchors.fill: parent
                            onClicked: {albumList.currentIndex = albumList.indexAt(menuItem.x, menuItem.y)
                            }
                            onDoubleClicked: {view.currentIndex=2}
                        }
                    }
                }
                ListView {
                    id: albumList
                    boundsBehavior: Flickable.DragAndOvershootBounds
                    anchors.fill: parent;
                    model: galleryItemModel
                    orientation: ListView.Vertical
                    flickDeceleration: 2000
                    cacheBuffer: 1000
                    highlightFollowsCurrentItem: true
                    highlight: Rectangle { color: "#00000000"; border {color:"#ffffff"; width:3} radius: 5}
                    currentIndex: -1

                    onCurrentIndexChanged: {
                        if(currentIndex!==-1)
                        {
                            highlightItem.width=currentItem.width+7;

                            IntelInterface.createImages(currentIndex);
                            //SaveandLoad.favcountry();

                        }
                    }
                }
            }
        }




        //Third hub page - the pictures
        Rectangle {
            id: galleryRect
            width: mainpagewindow.width; height: view.height
            color: "#00000000"
            opacity: 1


            Text {
                id: galleryText;

                color:textColor;

                font {family:skiWebWindow.police; bold: false; pointSize: 40}
                smooth: true

                style: Text.Raised
                anchors.leftMargin: 10
                anchors {top: parent.top; left:parent.left; topMargin:110}
                opacity:1
            }

            VisualDataModel {
                id: dataModel
                model: ListModel{
                    id:innerModel
                }

                delegate:

                    Image {
                    id: imagebro
                    width: photoGridView.cellWidth-10
                    height: photoGridView.cellHeight-10
                    sourceSize.width: photoGridView.cellWidth-10
                    sourceSize.height:photoGridView.cellHeight-10
                    clip: false
                    fillMode: Image.Stretch
                    source: url
                    BusyIndicator {
                        id: waiting_imagebro
                        x: (parent.width/2)-20
                        y: (parent.height/2)-20
                        width: 40
                        height: 40
                        running: true
                        visible: true
                    }
                    onStatusChanged: if (imagebro.status == Image.Ready){ waiting_imagebro.visible=false}


                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            console.log(PersistentData.albumsbig[countryList.currentIndex][albumList.currentIndex])
                            pageStack.push(Qt.resolvedUrl("ImageBig.qml"), { imagesource: imagebro.source, countryListcurrentIndex: countryList.currentIndex, albumListcurrentIndex:albumList.currentIndex});

                            //console.log("coucou"+ photoGridView.indexAt(parent.x, parent.y));
                            /*photoGridView.currentIndex = photoGridView.indexAt(parent.x, parent.y);
                            mainpagewindow.state="Image";
                            toolbar.state="Image";
                            if (photoGridView.currentItem!=null){
                                imagedetail.source=photoGridView.currentItem.source;
                            }*/
                        }
                    }
                }
            }

            GridView {
                id: photoGridView
                model: dataModel
                cacheBuffer: 5000

                anchors{
                    left: parent.left
                    leftMargin:10

                    right: parent.right
                    rightMargin: 10

                    bottom: parent.bottom
                    bottomMargin: 10

                    top: parent.top
                    topMargin: 200

                }


                cellWidth: (parent.width/2)-10
                cellHeight: cellWidth
                snapMode: GridView.NoSnap
                boundsBehavior: Flickable.StopAtBounds
                clip: true
            }

        }

    }

    ////////////////// GESTION DES VUES
    ListView {
        id: view
        visible:true

        model: itemModel
        orientation: ListView.Horizontal
        snapMode: ListView.SnapOneItem;
        flickDeceleration: 500
        cacheBuffer: 2000;

        boundsBehavior: Flickable.DragOverBounds

        property int previousX;

        x: 0

        width: mainpagewindow.width
        height: mainpagewindow.height
        contentWidth: 0



        onMovementStarted: previousX=contentX
        onContentXChanged:{ if ((view.contentX >= galleryRect.x) && (view.contentX <=(galleryRect.x+galleryRect.width-30)))
            {
                if (previousX>contentX && contentX<(galleryRect.x+50)) {
                    setSnapMode(true);
                }else {
                    setSnapMode(false);
                }
            } else {
                setSnapMode(true);
            }

            background.changex(((0-view.contentX)/2)-200);
            headingText.x=(0+view.contentX)/2;
        }


        function setSnapMode(on){
            if (on===true){
                view.snapMode=ListView.SnapOneItem;
                view.highlightRangeMode= ListView.StrictlyEnforceRange;
            } else if (on===false) {
                view.snapMode=ListView.NoSnap;
                view.highlightRangeMode= ListView.NoHighlightRange;
            }
        }
    }
}
