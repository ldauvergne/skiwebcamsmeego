import QtQuick 1.1
import com.nokia.meego 1.0
import QtMultimediaKit 1.1
import "javascript/PersistentData.js" as PersistentData

////////////////// IMAGE EN DETAIL
Page {
    id: rootZoomableImage
    orientationLock: PageOrientation.LockPortrait
    tools:mainbrowsingpagelayout1
    property string mailaddress: "skiwebcams@ovi.com"

    property string imagesource
    property string countryListcurrentIndex
    property string albumListcurrentIndex


    Background{
        id:background
        Component.onCompleted: background.changex(0);
    }

    property int photoHeight: 700
    property int photoWidth:2000

    Text{
        id: errorimagebigtext
        visible: false
        anchors{
            centerIn: parent
        }
        text:"Error while loading image,\nplease retry to load it."
        font.family:  skiWebWindow.police
        font.pixelSize: 25
        color: "#ffffff"
        styleColor: "#000000"
        wrapMode: TextEdit.Wrap

        horizontalAlignment: Text.AlignHCenter
        style: Text.Sunken
        font.bold: true
    }


    Flickable {
        id: flickable
        clip: true
        width:  parent.width
        height: parent.height
        contentWidth: imageContainer.width
        contentHeight: imageContainer.height
        onHeightChanged: photo.calculateSize()



        Item {
            id: imageContainer
            width: Math.max(photo.width * photo.scale, flickable.width)
            height: Math.max(photo.height * photo.scale, flickable.height)

            Image {
                id: photo
                property real prevScale
                source: imagesource
                cache: true
                anchors.centerIn: parent
                smooth: !flickable.movingVertically
                sourceSize.width: rootZoomableImage.photoWidth
                sourceSize.height:rootZoomableImage.photoHeight
                fillMode: Image.PreserveAspectFit

                function calculateSize()
                {
                    scale = Math.min(flickable.width / width, flickable.height / height) * 0.98;
                    pinchArea.minScale = scale;
                    prevScale = Math.min(scale, 1);
                }

                onStatusChanged: {
                    if ( photo.status == Image.Loading) {
                        indicator.visible = true;
                    } else
                        if ( photo.status == Image.Error ){
                            errorimagebigtext.visible=true; //console.log("Error while loading an image ");
                            indicator.visible = false;
                        }
                        else
                            if ( photo.status == Image.Ready) {
                                indicator.visible = false;
                                calculateSize();
                            }
                }

                onScaleChanged: {

                    if ((width * scale) > flickable.width) {
                        var xoff = (flickable.width / 2 + flickable.contentX) * scale / prevScale;
                        flickable.contentX = xoff - flickable.width / 2;
                    }
                    if ((height * scale) > flickable.height) {
                        var yoff = (flickable.height / 2 + flickable.contentY) * scale / prevScale;
                        flickable.contentY = yoff - flickable.height / 2;
                    }

                    prevScale = scale;

                }


            }
        }

        PinchArea{
            id: pinchArea
            property real minScale:  1.0
            anchors.fill: parent
            property real lastScale: 1.0
            pinch.target: photo
            pinch.minimumScale: minScale
            pinch.maximumScale: 3.0

            onPinchFinished: flickable.returnToBounds()
        }


        MouseArea {
            id: mousearea
            anchors.fill : parent
            property bool doubleClicked:  false


            onDoubleClicked: {

                mouse.accepted = true;
                console.log("Double clicked");
                if ( photo.scale > pinchArea.minScale){
                    photo.scale = pinchArea.minScale;

                    flickable.returnToBounds();
                }else{

                    photo.scale = 3.0;
                }
            }


        }
    }

    BusyIndicator {
        id: indicator
        x: (parent.width/2)-30
        y: (parent.height/2)-30
        width: 60
        height: 60
        running: true


    }

    ToolBarLayout {
        id:mainbrowsingpagelayout1
        visible: true
        ToolIcon {
            platformIconId: "toolbar-back"
            onClicked: {
                mainbrowsingpagemenu.close()
                pageStack.pop();
            }
        }
        /*
        ToolIcon {
            platformIconId: "toolbar-refresh2"
            onClicked: {

            }
        }
*/
        ToolIcon  {

            iconId: "toolbar-share"
            onClicked:{
                shareString.title="Ski Webcams ! N9"
                shareString.description=("Ski Webcams ! : Awsome weather in "+PersistentData.albumsbig[countryListcurrentIndex][albumListcurrentIndex])
                shareString.mimeType="text/x-url"
                shareString.text=imagesource
                shareString.share();

            }
        }
        ToolIcon {
            platformIconId: "toolbar-view-menu"
            anchors.right: (parent === undefined) ? undefined : parent.right
            onClicked: (mainbrowsingpagemenu.status === DialogStatus.Closed) ? mainbrowsingpagemenu.open() : mainbrowsingpagemenu.close()
        }
    }

    Menu {
        id: mainbrowsingpagemenu
        visualParent: pageStack
        MenuLayout {
            MenuItem { text: qsTr("About")
                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"));}
            MenuItem {
                text: "Report problem "
                onClicked:{ Qt.openUrlExternally("mailto:"+mailaddress+"?subject="+PersistentData.nompays[countryListcurrentIndex]+" with station "+(PersistentData.albumsbig[countryListcurrentIndex][albumListcurrentIndex])+"&body="+"Add your comment here or just press send. Please let the following link untouched.\n\n"+imagesource)}
            }
        }
    }
}
