import "javascript/IntelInterface.js" as IntelInterface
import "javascript/Storage.js" as Storage
import "javascript/SaveandLoad.js" as SaveandLoad
import "javascript/PersistentData.js" as PersistentData
import "javascript/IntelParser.js" as IntelParser
import "javascript/IntelFavourites.js" as IntelFavourites


import com.nokia.meego 1.0
import QtQuick 1.1
import QtMultimediaKit 1.1
import com.nokia.extras 1.0
Page {
    id: mainpagewindow
    orientationLock: PageOrientation.LockPortrait
    tools:mainbrowsingpagelayout1

    property color textColor: "#ffffff"






    HubBrowseViews{id:mainbrowser}
    Timer {
        interval: 700; running: true; repeat: false
        onTriggered: if (Storage.getSetting("RemainingPop")==0 && Storage.getSetting("gotofavs")=="false"){Storage.setSetting("gotofavs","true");}
                     else if (PersistentData.favourites.length>0 && Storage.getSetting("gotofavs")=="true"){pageStack.push(Qt.resolvedUrl("FavouritesBrowsingPage.qml"));}
    }

    ToolBarLayout {
        id:mainbrowsingpagelayout1
        visible: true
        ToolIcon {
            platformIconId: "toolbar-close"
            onClicked: {
                Qt.quit();
            }
        }
        ToolIcon {
            platformIconId: "toolbar-home"
            onClicked: {
                pageStack.push(Qt.resolvedUrl("FavouritesBrowsingPage.qml"));
            }
        }
        ToolIcon  {
            id:audiobutton
            iconId: "toolbar-mediacontrol-play"
            onClicked:{
                checklaunchaudio()

            }
        }
        ToolIcon {
            platformIconId: "toolbar-view-menu"
            anchors.right: (parent === undefined) ? undefined : parent.right
            onClicked: (mainbrowsingpagemenu.status === DialogStatus.Closed) ? mainbrowsingpagemenu.open() : mainbrowsingpagemenu.close()
        }
    }

    Menu {
        id: mainbrowsingpagemenu
        visualParent: pageStack
        MenuLayout {


            MenuItem { text: qsTr("About")
                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml")); }

            MenuItem {
                text: qsTr("Save as favourite")
                onClicked: {
                    if (mainbrowser.getselectedcountryindex() > 0 && mainbrowser.getselectedstationindex()>0){
                        IntelFavourites.savefav(mainbrowser.getselectedcountryindex(),mainbrowser.getselectedstationindex())

                    }
                    else
                    {
                        bannerMain.text="Please select a country & resort first."
                        bannerMain.iconSource="Warning.png"

                        bannerMain.show();
                    }

                }
            }
        }
    }
    InfoBanner {
        id:bannerMain
    }
    function checklaunchaudio() {
        if (playMusic.playing===false){
            playMusic.play()
            audiobutton.iconId="toolbar-mediacontrol-stop"
        }


        else{
            playMusic.stop()
            audiobutton.iconId="toolbar-mediacontrol-play"
        }

    }

    function checkerroraudio(){
        if (playMusic.error>0){
            dialogmediafail.open()
            audiobutton.iconId="toolbar-mediacontrol-play"
        }

    }

    Audio {
        id: playMusic

        autoLoad: false
        source: "http://st2.hotradio.fr:8072/"
        volume:0.7
        onStopped: audiobutton.iconId="toolbar-mediacontrol-play"
        onError: checkerroraudio()
    }

    QueryDialog {
        id: dialogmediafail
        titleText: "Streaming impossible"
        acceptButtonText: "Close"
        message: "Your connection doesn't support streaming "
    }


}


