

import QtQuick 1.1
import com.nokia.meego 1.0

Page {
    id: waitingpage
     orientationLock: PageOrientation.LockPortrait
    visible:true
    opacity:1

property string waittext:"Please wait while loading...\nMaybe there is no connectivity?\n\nIf so,please restart the app with\nconnectivity enabled:)\n3G/UMTS or Wifi recommended :)"



    Background{}

    Image{
        id:skiwebcamslogo
        scale: 0.9
        anchors{
            right: parent.right
            left: parent.left
            bottomMargin: 10
            bottom: work_.top
        }
        opacity: 1
        smooth:true
        source: "qrc:///skiwebcams.png" //ImageLoader.getRandomImage()



    }

    AnimationWaiting {
        id: work_
        x:parent.width/2
        y:parent.height/3
    }

    Text {
        id: waiting_text
        x: 0
        opacity:1
        anchors{
            topMargin:10
            top: work_.bottom
        }
        width: parent.width
        color: "#ffffff"
        text: waittext
        style: Text.Raised
        font.bold: true
        styleColor: "#000000"
        horizontalAlignment: Text.AlignHCenter
        font.family: skiWebWindow.police
        font.pixelSize: 25
    }







    ToolBar {
        id:toolbarerror
        opacity: 1
        anchors.bottom: parent.bottom
        tools: ToolBarLayout {

            ToolIcon {
                id:backbutton
                iconId: "toolbar-close"
                onClicked: Qt.quit();
            }
        }

    }

}
