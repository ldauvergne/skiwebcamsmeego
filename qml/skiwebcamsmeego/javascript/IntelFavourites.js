function Favourite(country, station) {
    this.country=country;
    this.station=station;
}



function isObjectsEquals(a, b) {
    var isOK = true;
    for (var prop in a) {
        if (a.hasOwnProperty(prop)) {
            if (a[prop] !== b[prop]) {
                isOK = false;
                break;
            }
        }
    }
    return isOK;
};

function savefav(country, station){
    var fav= new Favourite(country,station);

    var check=false;
    for(var i=0; i < PersistentData.favourites.length; i++){
        console.log(isObjectsEquals(PersistentData.favourites[i],fav))
        if (isObjectsEquals(PersistentData.favourites[i],fav)){
            check=true;
            break;
        }
    }

    if (check==false){
        PersistentData.favourites=PersistentData.favourites.concat(fav);
        Storage.setSetting("DateFavs",PersistentData.releasedatestring);
        savetoDB();
        bannerMain.text="Resort saved."
        bannerMain.iconSource="Refresh.png"

        bannerMain.show();
    }
    else{
        bannerMain.text="Favourite already saved."
        bannerMain.iconSource="Warning.png"
        bannerMain.show();
    }
}

function savetoDB(){
    Storage.setSetting("Favourites",JSON.stringify(PersistentData.favourites));
}

function getfromDB(){
    var favdbdata=Storage.getSetting("Favourites");

    if (favdbdata!=="Unknown")
        PersistentData.favourites= JSON.parse(favdbdata);
    else
        savetoDB();
}

function getfav(index){
    return [PersistentData.favourites[index].country,PersistentData.favourites[index].station];
}

function loadAllFavs()
{
    if (PersistentData.favourites.length===0){
        bannerFav.text="No favourites yet."
        bannerFav.iconSource="Warning.png"

        bannerFav.show();
    }


    if (PersistentData.releasedatestring !== Storage.getSetting("DateFavs"))
    {
        removeAllFavs()
        bannerFav.text="New data, please reselect favourites."
        bannerFav.iconSource="Warning.png"

        bannerFav.show();
    }

    albumFavModel.clear();

    //load the stations for country into persistent data
    for (var i=0; i<PersistentData.favourites.length;i++){

        if(PersistentData.countryloaded[PersistentData.favourites[i].country]===0){
            IntelParser.parsepays(PersistentData.favourites[i].country);
        }
        // console.log(PersistentData.favourites[i].country+ " "+ PersistentData.favourites[i].station);

        albumFavModel.append({"album":PersistentData.albumsbig[PersistentData.favourites[i].country][PersistentData.favourites[i].station]});


    }
}
function createFavImages(index)
{



    if(PersistentData.stationloaded[getfav(index)[0]][getfav(index)[1]]===0){
        IntelParser.parsestations(getfav(index)[0],getfav(index)[1]);
    }

    innerModel.clear();


    for (var i=0; i<PersistentData.webcam2D[getfav(index)[0]][getfav(index)[1]].length; i++){
        IntelInterface.createImage(PersistentData.webcam2D[getfav(index)[0]][getfav(index)[1]][i]);
    }

    galleryText.text=PersistentData.albumsbig[getfav(index)[0]][getfav(index)[1]];

}

function removeFavByIndex(arrayIndex){
    PersistentData.favourites.splice(arrayIndex,1);
    savetoDB();
    albumFavModel.remove(arrayIndex);
}

function removeAllFavs(){
    PersistentData.favourites=[];
    Storage.setSetting("DateFavs",PersistentData.releasedatestring);
    console.log()


    savetoDB();
    albumFavModel.clear();
}

/*function launch(){
    savefav(1, 2)
    savefav(3, 1)
    //console.log(getfav(1)[1])
    //console.log(PersistentData.favourites)
    //savetoDB();
    //getfromDB();
    //console.log(PersistentData.favourites)

}*/
