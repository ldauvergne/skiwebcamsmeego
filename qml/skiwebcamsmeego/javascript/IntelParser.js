

function getElementsByTagName(rootElement, tagName) {
    var childNodes = rootElement.childNodes;
    var elements = [];
    for (var i = 0; i < childNodes.length; i++) {
        if (childNodes[i].nodeName == tagName) {
            elements.push(childNodes[i]);
        }
    }
    return elements;
}

function parsestations(p,s){

    //On chope les stations courantes pour les parser
    PersistentData.station = getElementsByTagName(PersistentData.pays[p], 'station');
    PersistentData.stationloaded[p][s]=1;
    var w=0;
    PersistentData.webcam2D[p][s]=new Array();
    PersistentData.webcams = getElementsByTagName(PersistentData.station[s], 'imaweb');
    for (w in PersistentData.webcams) {
        PersistentData.webcam2D[p][s]=PersistentData.webcam2D[p][s].concat(PersistentData.webcams[w].firstChild.nodeValue)
    }
}


function parsepays(k){

    PersistentData.countryloaded[k]=1;
    //On chope les stations courantes pour avoir leur nom
    PersistentData.station = getElementsByTagName(PersistentData.pays[k], 'station');

    var i=0;

    PersistentData.webcam2D[k]=new Array();
    PersistentData.albumsbig[k]=new Array();
    for (i in PersistentData.station) {

        PersistentData.albumsbig[k][i]=getElementsByTagName(PersistentData.station[i], 'name')[0].firstChild.nodeValue;
        PersistentData.stationloaded[k]=PersistentData.stationloaded[k].concat(0);

    }
}



function initialize() {
    //waitingpage.state="Waiting";
    var req = new XMLHttpRequest();
    req.open('GET', 'http://pipould.free.fr/StationWeb/StationsDemo35.xml',true);
    //req.open('GET', 'StationsTEST.xml',true);
    req.send();

    req.onreadystatechange = function() {
                if (req.readyState === XMLHttpRequest.DONE) {
                    if(req.status == 200 && req.responseXML !== null){
                        var root = req.responseXML.documentElement;
                        PersistentData.apptitle=getElementsByTagName(root, 'titre')[0].firstChild.nodeValue
                        PersistentData.releasedatestring=getElementsByTagName(root, 'version')[0].firstChild.nodeValue



                        PersistentData.pays = getElementsByTagName(root, 'pays');
                        var k=0;

                        for (k in PersistentData.pays){
                            PersistentData.countryloaded=PersistentData.countryloaded.concat(0);
                            PersistentData.stationloaded[k]=new Array();
                            PersistentData.nompays=PersistentData.nompays.concat(getElementsByTagName(PersistentData.pays[k], 'nompays')[0].firstChild.nodeValue)



                        }


                        pageStack.replace(SaveandLoad.waitscreen())






                    }
                    else{
                        pageStack.replace(Qt.resolvedUrl("WaitingPage.qml"),{waittext:"Something went wrong.\n\nPlease restart the application\n\nOr check my support page\nwww.twistit.fr"})


                    }
                }
            }
}

