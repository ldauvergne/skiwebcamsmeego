import QtQuick 1.1
import com.nokia.meego 1.0
import "javascript/PersistentData.js" as PersistentData
import "javascript/IntelParser.js" as IntelParser
import "javascript/Storage.js" as Storage
import "javascript/SaveandLoad.js" as SaveandLoad
import "javascript/IntelFavourites.js" as IntelFavourites

PageStackWindow {
    id: skiWebWindow
    property string police: "Nokia Standard Light"
    Component.onCompleted: {
        pageStack.push(Qt.resolvedUrl("WaitingPage.qml"));
        Storage.initialize();
        IntelParser.initialize();
        IntelFavourites.getfromDB();


        PersistentData.MainBrowsingPageQml=Qt.createComponent("MainBrowsingPage.qml")


    }
}
