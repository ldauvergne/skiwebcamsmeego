# Add more folders to ship with the application, here
#folder_01.source = qml/skiwebcamsmeego
#folder_01.target = qml
#DEPLOYMENTFOLDERS = folder_01

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =


# If your application uses the Qt Mobility libraries, uncomment the following
# lines and add the respective components to the MOBILITY variable.
# CONFIG += mobility
# MOBILITY +=

# Speed up launching on MeeGo/Harmattan when using applauncherd daemon
CONFIG += qdeclarative-boostable
CONFIG += shareuiinterface-maemo-meegotouch share-ui-plugin share-ui-common

# Add dependency to Symbian components
# CONFIG += qt-components

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    sharestring.cpp

# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()

OTHER_FILES += \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/manifest.aegis \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog\
    qml/skiwebcamsmeego/main.qml \
    qml/skiwebcamsmeego/AboutPage.qml \
    qml/skiwebcamsmeego/AnimationWaiting.qml \
    qml/skiwebcamsmeego/Background.qml \
    qml/skiwebcamsmeego/FavouritesBrowsingPage.qml \
    qml/skiwebcamsmeego/HubBrowseViews.qml \
    qml/skiwebcamsmeego/ImageBig.qml \
    qml/skiwebcamsmeego/InfoSplashPage.qml \
    qml/skiwebcamsmeego/MainBrowsingPage.qml \
    qml/skiwebcamsmeego/WaitingPage.qml \
    qml/skiwebcamsmeego/ressources/Warning.png \
    qml/skiwebcamsmeego/ressources/skiwebcams.png \
    qml/skiwebcamsmeego/ressources/Refresh.png \
    qml/skiwebcamsmeego/ressources/backgroundtop2.jpg \
    qml/skiwebcamsmeego/ressources/backgroundtop.jpg \
    qml/skiwebcamsmeego/ressources/background.jpg \
    qml/skiwebcamsmeego/javascript/Storage.js \
    qml/skiwebcamsmeego/javascript/SaveandLoad.js \
    qml/skiwebcamsmeego/javascript/PersistentData.js \
    qml/skiwebcamsmeego/javascript/IntelParser.js \
    qml/skiwebcamsmeego/javascript/IntelInterface.js \
    qml/skiwebcamsmeego/javascript/IntelFavourites.js




HEADERS += \
    sharestring.h

RESOURCES += \
    resources.qrc
